django-gaccounts
================

``django-gaccounts`` is a `Django <https://djangoproject.com>`_ reusable application used to replace the default **Django** ``AUTH_MODEL`` as `recommended in their documentation <https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#extending-the-existing-user-model>`_.

It aims to manage **users** and **overrides default templates**. It provides a **login** and **register** view and template to manage your users registration.

.. warning:: This code is not production ready and still in active development. If you wish to contribute, consider joining us on `Github <https://github.com/guilieb/django-gaccounts>`_

As ``django-gaccounts`` is a Django reusable application, you can include it in your own Django project. You can use the :ref:`demo-server` that run the application alone on a sample Django project.


Introduction
============

.. toctree::
   :maxdepth: 4

   introduction

API Documentation
=================

.. toctree::
   :maxdepth: 4

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
