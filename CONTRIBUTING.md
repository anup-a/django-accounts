# Contribution guide

Please, refer to the [The Koala LMS Contribution Guide (on `develop`)](https://gitlab.com/koala-lms/lms/-/blob/develop/CONTRIBUTING.md)
