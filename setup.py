#!/usr/bin/env python3

import os

from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md'), encoding='utf-8') as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


def get_requirements():
    with open('requirements.txt') as requirements:
        req = requirements.read().splitlines()
    return req


exec(open('accounts/__init__.py').read())

# noinspection PyUnresolvedReferences
setup(
    name='django-koalalms-accounts',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    license='GPLv3 License',
    description='The Koala-LMS application to manage users.',
    long_description=README,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/koala-lms/django-accounts',
    author='Guillaume Bernard',
    author_email='guillaume.bernard@koala-lms.org',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 2.1',
        'Framework :: Django :: 2.2',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=get_requirements()
)
