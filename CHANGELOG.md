# Changelog for django-koalalms-accounts

## v1.2b1 − 2020.03.08

This is the first beta of version 1.2. It includes **15 commits** and received support from 1 contributor.He’s a student from the University of La Rochelle. Version 1.2b1 includes the following:

This version includes many fixes:
* `django-koalalms-learning` tracks only Django LTS 2.2.
* The account creation process needs a confirmation, sent by mail (Louis Barbier)
